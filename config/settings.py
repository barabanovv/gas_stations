import os
from dotenv import load_dotenv, find_dotenv
import psycopg2

load_dotenv(find_dotenv(), override=True, verbose=True)
# --- DATABASE SETTINGS
DB_URL = os.getenv('DB_URL')

conn_string = os.getenv('CONN_STRING')
conn = psycopg2.connect(conn_string)
