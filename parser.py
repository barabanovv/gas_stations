import requests

import lxml.html

from models.models import session, GasPrice, GasService, GazStation
from sqlalchemy.sql import exists


def save_html(html):
    with open('index.html', 'w') as f:
        f.write(html)


def get_html(url):
    r = requests.get(url)
    if r.ok:
        return r.text
    else:
        print(f'Status code: {r.status_code}')


def get_data(html):
    stations_path = '//div[@class="dotted azs_container"]'
    id_path = './div/a/span[starts-with(@id, "azs_number")]/@id'
    title_path = './div/a/span[starts-with(@id, "azs_number")]/text()'
    address_path = './div/div[starts-with(@id, "azs_address_")]/text()'
    gps_path = './div/div/div/nobr/span[contains(text(),"GPS:")]/../text()'
    working_hours_path = './div/div/div/nobr/span[contains(text(),"Режим работы:")]/../text()'
    prices_path = './div/div/div/nobr/span[contains(text(),"Топливо:")]/../div'
    gas_name_path = './div/text()'
    gas_price_path = './div/div[contains(text(), "₽")]/text()'
    services_path = './div/div/div/div[@class="serviceIcons"]/img'
    services_title_path = './@alt'

    tree = lxml.html.fromstring(html)
    stations = tree.xpath(stations_path)
    list_data = list()
    for i in stations:
        station_id = i.xpath(id_path)[0].strip().replace('azs_number_', '')
        title = i.xpath(title_path)[0].strip()
        address = i.xpath(address_path)[0].strip()
        gps = i.xpath(gps_path)[1].strip()
        working_hours = i.xpath(working_hours_path)[1].strip()
        prices = i.xpath(prices_path)
        price_list = {}
        for price in prices:
            gas_name = price.xpath(gas_name_path)[0].strip()
            try:
                gas_price = float(price.xpath(gas_price_path)[0].strip().replace('₽', ''))
            except:
                gas_price = None
            price_list[gas_name] = gas_price

        services = i.xpath(services_path)
        services_list = list()
        for service in services:
            services_title = service.xpath(services_title_path)[0]
            services_list.append(services_title)

        data = {
            'id': station_id,
            'title': title,
            'address': address,
            'gps': gps,
            'working_hours': working_hours,
            'price_list': price_list,
            'services': services_list
        }
        list_data.append(data)
    return list_data


def dump_data(data):
    for i in data:
        station_id = i['id']
        station = GazStation(id=station_id,
                             title=i['title'],
                             address=i['address'],
                             gps=i['gps'],
                             working_hours=i['working_hours'], )

        for j, k in i['price_list'].items():
            price = session.query(GasPrice).filter_by(gaz_station_id=station_id,
                                                      gas_name=j,
                                                      gas_price=k).first()
            if price:
                price.gas_name = j
                price.gas_price = k
            else:
                price = GasPrice(gaz_station_id=station_id,
                                 gas_name=j,
                                 gas_price=k)

                station.gas_price.append(price)

        for service in i['services']:
            exist_service = session.query(GasService).filter_by(gas_service=service).first()
            if exist_service:
                pass
            else:
                serv = GasService(gas_service=service)

                station.gas_service.append(serv)

        session.merge(station)
        session.commit()


def main():
    url = 'https://www.gpnbonus.ru/our_azs/?region_id=139'
    data = get_data(get_html(url))
    dump_data(data)


if __name__ == '__main__':
    main()
    # save_html(get_html('https://www.gpnbonus.ru/our_azs/?region_id=139'))
