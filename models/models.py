from sqlalchemy import create_engine, Column, Integer, String, DECIMAL, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from config.settings import DB_URL

engine = create_engine(DB_URL, echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()

services_association = Table(
    'services_association', Base.metadata,
    Column('gas_station_id', Integer, ForeignKey('gas_station.id'), primary_key=True),
    Column('gas_service_id', Integer, ForeignKey('gas_service.id'), primary_key=True),
)

session = Session()


class GazStation(Base):
    __tablename__ = 'gas_station'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    address = Column(String)
    gps = Column(String)
    working_hours = Column(String)

    gas_service = relationship("GasService",
                               secondary=services_association,
                               back_populates='gas_stations')

    def __repr__(self):
        return f'<GazStation({self.title}: {self.id}")>'


class GasPrice(Base):
    __tablename__ = 'gas_price'

    id = Column(Integer, primary_key=True)
    gaz_station_id = Column(Integer, ForeignKey("gas_station.id"))
    gas_name = Column(String)
    gas_price = Column(DECIMAL)

    def __repr__(self):
        return f"<GasPrice({self.gas_name}: {self.gas_price})>"


class GasService(Base):
    __tablename__ = 'gas_service'

    id = Column(Integer, primary_key=True)
    gas_service = Column(String, unique=True)
    gas_stations = relationship("GazStation",
                                secondary=services_association,
                                back_populates='gas_service',)

    def __repr__(self):
        return f"<GasService({self.gas_service}: {self.gas_stations})>"


Base.metadata.create_all(engine)
